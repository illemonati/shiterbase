use std::fs::File;
use std::path::PathBuf;

pub struct DataBase {
    pub(crate) file_path: PathBuf,
    pub(crate) file_handle: File
}