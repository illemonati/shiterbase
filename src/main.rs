


mod fsio;
mod db;
mod interpreter;
mod repl;
mod parser;
mod token_type;
mod scanner;
mod token_literal_type;
mod token;
mod expr;
mos stmt;


#[macro_use]
extern crate derive_more;

use clap::Clap;
use repl::Repl;


#[derive(Clap)]
#[clap(version = "0.0.1", author = "illemonati <tonymiaotong@tioft.tech>")]
struct Opts {
    file_path: String,
}


fn main() {
    let opts: Opts = Opts::parse();
    let db = fsio::open_or_create_database(&opts.file_path).expect("Error opening database");
    println!("Opened database at {}", db.file_path.to_str().expect("Path to str err"));
    let mut repl = Repl::new(db);
    repl.start();
}





