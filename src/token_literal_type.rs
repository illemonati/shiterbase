use std::fmt;
use ordered_float::NotNan;


#[derive(Debug, PartialEq, Display, Clone, Eq)]
pub enum TokenLiteralType {
    #[display(fmt = "{}", _0)]
    String(String),
    #[display(fmt = "{}", _0)]
    Number(NotNan<f64>),
    #[display(fmt = "{}", _0)]
    Bool(bool),
    #[display(fmt = "nil")]
    Nil,
    #[display(fmt = "{}", _0)]
    Json(serde_json::Value),
}

impl TokenLiteralType {
    pub fn is_truthy(&self) -> bool {
        match self {
            Self::Nil => false,
            Self::Bool(b) => *b,
            Self::String(_s) => true,
            Self::Number(_n) => true,
            Self::Json(_j) => true,
        }
    }

    pub fn is_equal(&self, other: Self) -> bool {
        match self {
            Self::String(s) => {
                if let Self::String(os) = other {
                    return *s == os;
                }
            },
            Self::Json(s) => {
                if let Self::Json(val) = other {
                    return *s == val;
                }
            },
            Self::Number(n) => {
                if let Self::Number(on) = other {
                    return *n == on;
                }
            },
            Self::Bool(b) => {
                if let Self::Bool(ob) = other {
                    return *b == ob;
                }
            },
            Self::Nil => {
                if let Self::Nil = other {
                    return true;
                }
            },
        };
        return false;
    }
}
