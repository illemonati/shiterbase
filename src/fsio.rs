use std::fs::{self, File, OpenOptions};
use std::io;
use crate::db::DataBase;

pub fn open_or_create_database(database_path: &str) -> Result<DataBase, io::Error> {
    println!("Try opening or creating database at {}", database_path);
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(database_path);
    match file {
        Ok(file) => Ok(DataBase {
            file_path: fs::canonicalize(database_path).expect("Canonicalize path error"),
            file_handle: file
        }),
        Err(e) => Err(e)
    }
}



