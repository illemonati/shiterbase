use crate::db::DataBase;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use std::io::Write;
use crate::scanner::Scanner;


pub struct Repl {
    db: DataBase,
    rl: Editor<()>,
}


impl Repl {
    pub fn new(db: DataBase) -> Self {
        let rl = Editor::<()>::new();
        Repl {
            db,
            rl,
        }
    }

    pub fn start(&mut self) {
        let prompt_str = format!("shiterbase ({})>> ", self.db.file_path.file_name()
            .expect("Error getting file name")
            .to_str()
            .expect("Error converting OS str")
        );
        loop {
            let readline = self.rl.readline(&prompt_str);
            match readline {
                Ok(line) => {
                    self.rl.add_history_entry(line.as_str());
                    self.process_line(&line);
                }
                Err(ReadlineError::Interrupted) => {
                    println!("CTRL-C: Stopping");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("CTRL-D: Stopping");
                    break;
                }
                Err(err) => {
                    println!("Error: {:?}", err);
                    break;
                }
            }
        }
    }


    fn process_line(&self, line: &str) {
        println!("{}", line);
        let mut scanner = Scanner::new(line);
        let scanner_res = scanner.scan_tokens();
        match scanner_res {
            Ok(scanner_res) => {
                println!("{:?}", scanner_res);
            }
            Err(e) => {
                println!("Scanner Error: {} at line {}", e.msg, e.line)
            }
        }
    }
}


